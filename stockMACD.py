# -*- coding: utf-8 -*-
"""
Created on Fri Aug  4 12:11:31 2017

@author: kutty
"""


import pandas as pd 

px = pd.read_csv("test.txt")


px['26 ema'] = pd.ewma(px["factor_1"], span=26) 
px['12 ema'] = pd.ewma(px["factor_1"], span=12) 
px['MACD'] = (px['12 ema'] - px['26 ema'])
px['Signal Line'] = pd.ewma(px['MACD'], span=9)


px.plot(y=['factor_1'], title='Close')
px.plot(y= ['MACD', 'Signal Line'], title='MACD & Signal Line')

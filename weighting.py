from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
import matplotlib.pyplot as plt

'''
Create TF-IDF cosine similarity between queries and all documents

Inputs:
* terms: clustering terms to investigate
* file_name: path to csv containing original tweets
* append_query: set to TRUE to treat all terms as one query. Otherwise they will be treated individually

Output:
* df: pandas DataFrame with TF-IDF cosine similarity
* queries: queries, appended if append_query=TRUE
'''
def tf_idf_summary(terms, file_name='geneSampleSet.csv', append_query=True):
    df = pd.read_csv(file_name)

    tf_idf_vect = TfidfVectorizer(strip_accents='unicode', stop_words='english', ngram_range=(1,2))
    X_train_counts = tf_idf_vect.fit_transform(df['Tweet'])

    queries = []
    if (append_query):
        queries.append(' '.join(terms))
    else:
        queries = terms

    query_counts = tf_idf_vect.transform(queries)
    sim = cosine_similarity(query_counts, X_train_counts)

    for i in range(len(queries)):
        df[queries[i]] = pd.Series(sim[i])

    return df, queries

'''
Create BM25 weighting schemes between queries and all documents

Inputs:
* terms: clustering terms to investigate
* file_name: path to csv containing original tweets
* append_query: set to TRUE to treat all terms as one query. Otherwise they will be treated individually

Output:
* df: pandas DataFrame with BM25 weighting of every query on each document
* queries: queries, appended if append_query=TRUE
'''

def bm25_summary(terms, file_name='geneSampleSet.csv', append_query=True, k1=2.0, b=0.75):
    df = pd.read_csv(file_name)
    M = len(df)

    # find tfs
    tf_vect = CountVectorizer(strip_accents='unicode', stop_words='english', ngram_range=(1,2))
    X_train_counts = tf_vect.fit_transform(df['Tweet'])

    # find idfs
    tf_idf_vect = TfidfTransformer()
    tf_idf_vect.fit(X_train_counts)

    # calculate preprocessed document length
    doc_len = [x.sum() for x in X_train_counts]
    avg_doc_len = sum(doc_len)/float(M)

    # prep query
    queries = []
    if (append_query):
        queries.append(' '.join(terms))
    else:
        queries = terms

    # TF of queries
    query_counts = tf_vect.transform(queries)


    for q in range(len(queries)):
        term_idx = query_counts[q].nonzero()[1]
        bm25_q = [0] * M

        for i in range(M):
            A = (k1+1)
            B = k1 * (1 - b + (b * doc_len[i] / avg_doc_len))
            for j in term_idx:
                idf_q = tf_idf_vect.idf_[j]
                fqd = X_train_counts[i,j]

                bm25_q[i] += ((idf_q * fqd * A) / (fqd + B))

        df[queries[q]] = bm25_q

    return df, queries

'''
Create MACD and signal plot

Inputs (from tf_idf_summary):
* df: pandas DataFrame with TF-IDF cosine similarity
* queries: queries, appended if append_query=TRUE
* use mean: set to TRUE to use mean grouping instead of sum. Set to False by default
* use month: set to TRUE to group by year + month. Use year is ignored if set to TRUE.
* use_year: set to TRUE to group by year.

Output:
* None

Opens pop-up windows of matplotlib.pyplot plots
'''
def macd_plot(df, queries, use_mean = False, use_month=False, use_year=False):

    # year/month grouping
    df['Year'] = pd.DatetimeIndex(df['Date']).year
    df['Month'] = pd.DatetimeIndex(df['Date']).month
    gp = df.groupby(df['Date'])

    if (use_month):
        gp = df.groupby((df['Year'], df['Month']))
    elif (use_year):
        gp = df.groupby(df['Year'])

    # mean vs sum
    px = pd.DataFrame(gp[queries].sum())
    title_str = 'Sum'

    if (use_mean):
        px = pd.DataFrame(gp[queries].mean())
        title_str = 'Mean'

    # reset index
    px = px.reset_index()

    for query in queries:
        px['26 ema {}'.format(query)] = pd.ewma(px[query], span=26)
        px['12 ema {}'.format(query)] = pd.ewma(px[query], span=12)
        px['MACD {}'.format(query)] = (px['12 ema {}'.format(query)] - px['26 ema {}'.format(query)])
        px['Signal Line {}'.format(query)] = pd.ewma(px['MACD {}'.format(query)], span=9)


        x_series = pd.Series()
        if (use_month):
            x_yr = px['Year'].astype(str)
            x_mo = px['Month'].astype(str)
            x_series = x_yr.str.cat(x_mo, sep=',')
        elif (use_year):
            x_series = px['Year']
        else:
            x_series = px['Date']

        px.plot(y=[query], x=x_series, title='{} of weight for query "{}"'.format(title_str, query))
        px.plot(y=['MACD {}'.format(query), 'Signal Line {}'.format(query)], x=x_series, title='MACD & Signal Line for query "{}" per day'.format(query))
        plt.show()

if __name__ == '__main__':
    df, queries = tf_idf_summary(['gmo', 'crispr', 'gene editing', 'genome editing'], append_query=True)
    macd_plot(df, queries, use_mean=True, use_month=True)
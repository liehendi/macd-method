import re # regex module
import pandas as pd
from nltk.tokenize import TweetTokenizer

'''
Preprocess the original sample text and convert it to csv
'''

def preprocess_original_text(file_name='geneSampleSet.txt', save_name='geneSampleSet.csv', use_stem = False):
    file = open(file_name, 'r')
    content = file.read()
    file.close()

    splits = re.split('[\t\n]', content)

    date_regex = re.compile('\d+\/\d+\/\d{4}')
    link_regex = re.compile('(http\S+)')
    # word_regex = re.compile('[\w\d]+')


    pairs = []
    cur_date = ""
    cur_tweet_str = ""

    tknzr = TweetTokenizer(strip_handles=True, reduce_len=True, preserve_case=False)

    for s in splits:
        if (date_regex.match(s)):
            if ((len(cur_date) > 0) and (len(cur_tweet_str) > 0)):
                cur_tweet_str = link_regex.sub('', cur_tweet_str)
                cur_tweet_str.replace('#', '')
                tokens = tknzr.tokenize(cur_tweet_str)
                cur_tweet_str = ' '.join(tokens)
                pairs.append((cur_date, cur_tweet_str))
            cur_date = s
            cur_tweet_str = ""
        else:
            cur_tweet_str += s
            cur_tweet_str += " "

    if ((len(cur_date) > 0) and (len(cur_tweet_str) > 0)):
        cur_tweet_str = link_regex.sub('', cur_tweet_str)
        cur_tweet_str = ' '.join(tknzr.tokenize(cur_tweet_str))
        pairs.append((cur_date, cur_tweet_str))

    df = pd.DataFrame(pairs)
    df.columns = ['Date', 'Tweet']
    df['Date'] = pd.to_datetime(df['Date'], dayfirst=True)
    df.to_csv(save_name)